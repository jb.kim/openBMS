We will share the battery management system used in the energy storage system, except for some confidentiality areas.
The scope of sharing is circuit diagram, firmware, communication protocol, etc., through which you can immediately manage a system using a large amount of batteries.
We hope that this open source will further energize the energy storage system and its associated markets.


from JB